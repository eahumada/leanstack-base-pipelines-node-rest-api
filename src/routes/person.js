let mongoose = require('mongoose');
let Person = require('../model/person');

function personPost(req, res) {
    console.log('Request arrived.');

    console.log('Person name='+req.body.name);

    var person = new Person();      // create a new instance of the People model

    person.name = req.body.name;  // set the people name (comes from the request)

    // save the bear and check for errors
    console.log('Saving Bear...');

    person.save(function(err) {

        console.log('Person Saved.');

        if (err)
            res.send(err);

        res.json({ message: 'Person created!' });

        console.log('Person Created.');
    });

    console.log('Request dispatched.');
}

// get all the bears (accessed at GET http://localhost:8080/api/people)
function personFind(req, res) {
        Person.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });
}

// on routes that end in /bears/:bear_id
// ----------------------------------------------------
function personFindById(req, res) {
        Person.findById(req.params.person_id, function(err, person) {
            if (err)
                res.send(err);
            res.json(person);
        });
}

// update the bear with this id (accessed at PUT http://localhost:8080/api/people/:person_id)
function personPutById(req, res) {

        // use our bear model to find the bear we want
        Person.findById(req.params.person_id, function(err, person) {

            if (err)
                res.send(err);

            person.name = req.body.name;  // update the bears info

            // save the bear
            person.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Bear updated!' });
            });

        });
}
    
// delete the bear with this id (accessed at DELETE http://localhost:8080/api/people/:person_id)
function personDeleteById(req, res) {
    Person.remove({
        _id: req.params.person_id
    }, function(err, person) {
        if (err)
            res.send(err);

        res.json({ message: 'Successfully deleted' });
    });
}

//export all the functions
module.exports = { personPost, personFind, personFindById, personPutById, personDeleteById };
