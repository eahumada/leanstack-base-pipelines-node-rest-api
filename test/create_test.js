//inside create_test.js
const assert = require('assert');
const Model = require('../src/model/person'); //imports the Pokemon model.
describe('Creating documents', () => {
    it('creates a person', (done) => {
        //assertion is not included in mocha so 
        //require assert which was installed along with mocha
        const item = new Model({ name: 'Doe, John' });
        item.save() //takes some time and returns a promise
            .then(() => {
                assert(!item.isNew); //if poke is saved to db it is not new
                done();
            });
    });
});