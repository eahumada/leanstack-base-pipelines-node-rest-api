// delete_test.js
const assert = require('assert');
const Pokemon = require('../src/model/person');
describe('Deleting a person', () => {

  let poke;

  beforeEach((done) => {
    poke = new Pokemon({ name: 'poke' });
    poke.save()
      .then(() => done());
  });

  it('removes a person using its instance', (done) => {
    poke.remove()
      .then(() => Pokemon.findOne({ name: 'poke' }))
      .then((person) => {
        assert(person === null);
        done();
      });
  });

  it('removes multiple pokemons', (done) => {
    Pokemon.remove({ name: 'poke' })
      .then(() => Pokemon.findOne({ name: 'poke' }))
      .then((person) => {
        assert(person === null);
        done();
      });
  });

  it('removes a person', (done) => {
    Pokemon.findOneAndRemove({ name: 'poke' })
      .then(() => Pokemon.findOne({ name: 'poke' }))
      .then((person) => {
        assert(person === null);
        done();
      });
  });

  it('removes a person using id', (done) => {
    Pokemon.findByIdAndRemove(poke._id)
    // the following code block is repeated again and again
      .then(() => Pokemon.findOne({ name: 'poke' }))
      .then((person) => {
        assert(person === null);
        done();
      });
    // block end
  })
})
