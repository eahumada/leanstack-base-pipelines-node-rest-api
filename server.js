// server.js

// BASE SETUP

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

mongoose.connect('mongodb://localhost:27017/persons', {useNewUrlParser: true}); // connect to our database

var person = require('./src/routes/person');

//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

// more routes for our API will happen here

// on routes that end in /bears
// ----------------------------------------------------
router.route('/people')
    // create a bear (accessed at POST http://localhost:8080/api/bears)
    .post(person.personPost)    
    // get all the bears (accessed at GET http://localhost:8080/api/bears)
    .get(person.personFind);

// on routes that end in /persons/:person_id
// ----------------------------------------------------
router.route('/people/:person_id')
    // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:person_id)
    .get(person.personFindById)
    // update the bear with this id (accessed at PUT http://localhost:8080/api/bears/:person_id)
    .put(person.personPutById)    
    // delete the bear with this id (accessed at DELETE http://localhost:8080/api/bears/:person_id)
    .delete(person.personDeleteById);

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
app.listen(port);

console.log('Magic happens on port ' + port);
